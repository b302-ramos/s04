-- 1. Find all artists that has letter d in its name.
SELECT *
FROM artists
WHERE name LIKE '%d%';

-- 2. Find all songs that has a length of less than 3:50.
SELECT *
FROM songs
WHERE length < TIME('00:03:50');

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT a.album_title, s.song_name, s.length
FROM albums a
INNER JOIN songs s ON a.id = s.album_id;

-- 4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT albums.id, albums.album_title, albums.date_released, artists.name
FROM albums
JOIN artists ON albums.artist_id = artists.id
WHERE albums.album_title LIKE '%a%';

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT *
FROM albums
ORDER BY album_title DESC
LIMIT 4;

-- 6. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT a.album_title, s.song_name
FROM albums a
JOIN songs s ON a.id = s.album_id
ORDER BY a.album_title DESC;
